#pragma once
//#include "builddisp.h"
#include "mathlib/mathlib.h"
#include "tier1_lib/bitvec.h"
#include "tier1_lib/utlvector.h"
#include "tier1_lib/utlbuffer.h"
#include "tier1_lib/utlarray2d.h"
#include "tier1_lib/utlstring.h"
#include "tier2/bspflags.h"

//#include "tier2/bspfile.h" //Temporaly

// Generates lump declaration
#define DECLARE_BSP_LUMP(ID, NAME) \
	static const uint NAME##Id = ID; \
	struct NAME##Lump;


#define DECLARE_READ_WRITE(TYPE) \
	static bool Read(CUtlBuffer& binary, TYPE& out); \
	void Write(CUtlBuffer& binary);

namespace bsp_v18 {
	struct DisplacementInfoImplicit;
}

struct I16BBox
{
	int16 Mins[3];
	int16 Maxs[3];
};

static constexpr uint32 DisplacementVertexesByPower(uint8 power) noexcept 
{
	return Square((1 << power) + 1);
}
		
static constexpr uint32 DisplacementTrianglesByPower(uint8 power) noexcept 
{
	return 2 * Square(1 << power);
}

struct BspV18
{
	
	static const uint8 MAX_LIGHTMAPS = 4;
	using Lightstyle = byte;
	
	DECLARE_BSP_LUMP( 0, Entities)
	DECLARE_BSP_LUMP( 1, Planes)
	using PlaneIndex = uint16;
	DECLARE_BSP_LUMP( 2, TextureData)
	using TextureDataIndex = uint32;
	DECLARE_BSP_LUMP( 3, Vertexes)
	using VertexIndex = uint16;
	DECLARE_BSP_LUMP( 4, Visibility)
	DECLARE_BSP_LUMP( 5, Nodes)
	using NodeIndex = uint32;
	DECLARE_BSP_LUMP( 6, TextureInfo)
	using TextureInfoIndex = uint16;
	DECLARE_BSP_LUMP( 7, Faces)
	using FaceIndex = uint16;
	DECLARE_BSP_LUMP( 8, LightingData)
	using LightingDataIndex = uint32;
	DECLARE_BSP_LUMP( 9, Occlusion)
	DECLARE_BSP_LUMP(10, Leafs)
	using LeafIndex = uint32; // TODO: correct type?
	// 11 skiped
	DECLARE_BSP_LUMP(12, Edges)
	using EdgeIndex = int32;
	DECLARE_BSP_LUMP(13, SurfaceEdges)
	DECLARE_BSP_LUMP(14, BrushModels)
	using BrushModelIndex = uint16;
	DECLARE_BSP_LUMP(15, WorldLights)
	DECLARE_BSP_LUMP(16, LeafFaces)
	using LeafFaceIndex = uint16;
	DECLARE_BSP_LUMP(17, LeafBrushes)
	using LeafBrushIndex = uint16;
	DECLARE_BSP_LUMP(18, Brushes)
	using BrushIndex = uint16;
	DECLARE_BSP_LUMP(19, BrushSides)
	using BrushSide = uint32;
	DECLARE_BSP_LUMP(20, Areas)
	using AreaIndex = uint16;
	DECLARE_BSP_LUMP(21, Areaportals)
	using AreaportalIndex = uint32;
	DECLARE_BSP_LUMP(22, Portals)
	using PortalIndex = uint32;
	DECLARE_BSP_LUMP(23, Clusters)
	using ClusterIndex = uint16;
	DECLARE_BSP_LUMP(24, PortalVertexes)
	using PortalVertexIndex = uint32;
	DECLARE_BSP_LUMP(25, ClusterPortals)
	using ClusterPortalIndex = uint32;
	DECLARE_BSP_LUMP(26, DisplacementInfo)
	using DisplacementInfoIndex = uint16;
	DECLARE_BSP_LUMP(27, OriginalFaces)
	using OriginalFaceIndex = int32;
	// 28 skipped
	DECLARE_BSP_LUMP(29, PhysicalCollisions)
	DECLARE_BSP_LUMP(30, VertexNormals)
	using VertexNormalIndex = uint16;
	DECLARE_BSP_LUMP(31, VertexNormalIndices)
	DECLARE_BSP_LUMP(32, DisplacementLightmapAlpha)
	using DisplacementLightmapAlphaIndex = uint32;
	DECLARE_BSP_LUMP(33, DisplacementVertexes)
	using DisplacementVertexIndex = uint32;
	DECLARE_BSP_LUMP(34, DisplacementLightmapSamplePositions)
	using DisplacementLightmapSamplePositionIndex = uint32;
	DECLARE_BSP_LUMP(35, Custom)
	DECLARE_BSP_LUMP(36, LeafWaterData)
	using LeafWaterDataIndex = uint16;
	DECLARE_BSP_LUMP(37, Primitives)
	using PrimitiveIndex = uint16;
	DECLARE_BSP_LUMP(38, PrimitiveVertexes)
	using PrimitiveVertexIndex = uint16;
	DECLARE_BSP_LUMP(39, PrimitiveIndices)
	using PrimitiveIndexIndex = uint16;
	DECLARE_BSP_LUMP(40, PakFile)
	DECLARE_BSP_LUMP(41, ClipPortalVertexes)
	using ClipPortalVertexIndex = uint16;
	DECLARE_BSP_LUMP(42, Cubemaps)
	DECLARE_BSP_LUMP(43, TextureStringData)
	using TextureStringDataIndex = uint32;
	DECLARE_BSP_LUMP(44, TextureStringTable)
//	using TextureStringTableIndex = uint32;
	DECLARE_BSP_LUMP(45, Overlays)
	DECLARE_BSP_LUMP(46, LeafMinDistToWater)
	DECLARE_BSP_LUMP(47, FaceMacroTextureInfo)
	DECLARE_BSP_LUMP(48, DisplacementTrianglesTags)
	using DisplacementTriangleIndex = uint32;

	struct EntitiesLump
	{
		DECLARE_READ_WRITE(EntitiesLump)
	} Entities; // TODO:

	struct PlanesLump
	{
		struct Plane
		{
			Vector Normal;
			float Distance;
			int Type;
		};

		CUtlVector<Plane> Data;
		
		DECLARE_READ_WRITE(PlanesLump)
	} Planes;

	struct TextureDataLump
	{
		struct TextureDataElement
		{
			Vector Reflectivity;
			TextureStringDataIndex Name;
			uint TextureWidth, TextureHeight;

			uint ViewWidth, ViewHeight; // Unused
		};

		CUtlVector<TextureDataElement> Data;
		
		DECLARE_READ_WRITE(TextureDataLump)
	} TextureData;

	struct VertexesLump
	{
		CUtlVector<Vector> Vertexes;

		DECLARE_READ_WRITE(VertexesLump)
	} Vertexes;

	struct VisibilityLump
	{
		CUtlVector<byte> Binary; //TODO: parse

		DECLARE_READ_WRITE(VisibilityLump)
	} Visibility;

	struct NodesLump
	{
		struct Node
		{
			union {
				PlaneIndex Plane;
				uint32 PlaneRaw;
			};
			
			union Child
			{
				NodeIndex Node;
				LeafIndex Leaf;
				int32 RawIndex = 0; // if (RawIndex > 0) { Node = RawIndex) } else { Leaf = -RawIndex - 1 }
			};
			Child Children[2];

			I16BBox BoundingBox;
			
			FaceIndex FaceFirst;
			uint16 FaceCount; // From FaceFirst to FaceFirst + FaceCount

			union
			{
				AreaportalIndex AreaPortal;
				uint16 AreaPortalRaw = ~0; // ~0 == -1 -> no areaportal here
			};
			
		};
		CUtlVector<Node> Data;

		DECLARE_READ_WRITE(NodesLump)
	} Nodes;

	struct TextureInfoLump
	{
		// Coord = Pos * WorldScale + Constant
		// Coord may be u, v, lightmapU, lightmapV
		struct WorldToTexsize
		{
			Vector WorldScale;
			vec_t Constant;
		};
		
		struct TextureInfo
		{
			WorldToTexsize TextureU, TextureV; // U = S, V = T
			WorldToTexsize LightmapU, LightmapV; // U = S, V = T
			union
			{
				SurfaceFlags SurfaceFlags;
				uint32 SurfaceFlagsRaw; // Engine uses 16 bit short, but BSP uses 32-bit int
			};
			TextureDataIndex TextureData;
		};

		CUtlVector<TextureInfo> Data;
		
		DECLARE_READ_WRITE(TextureInfoLump)
	} TextureInfo;

	struct FacesLump
	{
		struct Face
		{
			colorRGBExp32 AvgLightColor[MAX_LIGHTMAPS]; // TODO: BSP_VERSION_CHANGE: remove in v19

			PlaneIndex Plane;
			bool8 DirOppositeToPlane;
			bool8 OnNode; // True - face is on node; false - face is in leaf

			EdgeIndex EdgeFirst;
			uint16 EdgeCount; // From EdgeFirst to EdgeFirst + EdgeCount

			TextureInfoIndex Texture;

			DisplacementInfoIndex Displacement;

			// This is only for surfaces that are the boundaries of fog volumes
			// (ie. water surfaces)
			// All of the rest of the surfaces can look at their leaf to find out
			// what fog volume they are in.
			LeafWaterDataIndex FogVolume;

			Lightstyle Styles[MAX_LIGHTMAPS]; //TODO: probably an index

			LightingDataIndex LightingDataStart; // start of [numstyles*surfsize] samples

			float Area = 0;
				
			int32 TextureUMin, TextureVMin;
			int32 TextureUSize, TextureVSize;

			OriginalFaceIndex OriginalFace;

			uint16 PrimitivesCount;
			PrimitiveIndex PrimitivesStart; // PrimitivesStart to PrimitivesStart + PrimitivesCount

			uint32 SmoothingGroups; // Bitfield
		};

		CUtlVector<Face> Data;
		
		DECLARE_READ_WRITE(FacesLump)
	} Faces;

	struct LightingDataLump
	{
		CUtlVector<colorRGBExp32> Colors;

		DECLARE_READ_WRITE(LightingDataLump)
	} LightingData;

	struct OcclusionLump
	{
		using OcculderPolygonIndex = uint32;
		using OcculderVertexIndex = uint32;
		
		struct OcculderData
		{
			enum OcculderFlags: uint32
			{
				OcculderInactive = 0x1
			} Flags;

			OcculderPolygonIndex PolygonFirst;
			uint32 PolygonCount;

			Vector BoundMins, BoundMaxs;
		};
		CUtlVector<OcculderData> Occulders;

		struct OcculderPolygon
		{
			OcculderVertexIndex VertexFirst;
			uint32 VertexCount;
			PlaneIndex Plane;
			
		};
		CUtlVector<OcculderPolygon> Polygons;

		union OcclusionVertexIndex
		{
			VertexIndex Index;
			uint32 Raw;
		};
		
		CUtlVector<OcclusionVertexIndex> VertexIndices;
		

		DECLARE_READ_WRITE(OcclusionLump)
	} Occlusion;

	struct LeafsLump
	{
		struct Leaf
		{
			ContentsFlag Contents;
			ClusterIndex Cluster;
			AreaIndex Area;

			I16BBox BoundingBox;

			LeafFaceIndex FaceFirst;
			uint16 FaceCount;

			LeafBrushIndex BrushFirst;
			uint16 BrushCount;

			LeafWaterDataIndex WaterData; // ~0 -> not in water
		};

		CUtlVector<Leaf> Data;

		DECLARE_READ_WRITE(LeafsLump)
	} Leafs;

	struct EdgesLump
	{
		struct Edge
		{
			VertexIndex Vertex1, Vertex2;
		};

		CUtlVector<Edge> Data;

		DECLARE_READ_WRITE(EdgesLump)
	} Edges;

	struct SurfaceEdgesLump
	{
		CUtlVector<EdgeIndex> Edges; // Positive -> edge first to second, negative -> edge second to first

		DECLARE_READ_WRITE(SurfaceEdgesLump)
	} SurfaceEdges;

	struct BrushModelsLump
	{
		struct BrushModel
		{
			Vector BoundMin, BoundMax;
			Vector Origin;

			NodeIndex HeadNode;

			union
			{
				FaceIndex FaceFirst;
				uint32 FaceFirstRaw;
			};

			uint32 FaceCount;	
		};

		CUtlVector<BrushModel> Data;

		DECLARE_READ_WRITE(BrushModelsLump)
	} BrushModels;

	struct WorldLightsLump
	{
		struct WorldLight
		{
			Vector Origin;
			Vector ColorAndIntensity;
			Vector Direction;

			union
			{
				ClusterIndex Cluster;
				uint32 ClusterRaw;
			};

			enum class LightType: uint32
			{
				Surface, // 90 degree spotlight
				Point, // simple point light source
				Spotlight, // spotlight with penumbra
				Skylight, // directional light with no falloff (surface must trace to SKY texture)
				QuakeLight, // linear falloff, non-lambertian
				SkyAmbient // spherical light source with no falloff (surface must trace to SKY texture)
			} Type;

			union
			{
				Lightstyle Style;
				uint32 StyleRaw;
			};

			float StopDot1, StopDot2; // Start and end of penumbra for emit_spotlight

			float Exponent;
			float MaxRadius;

			// brightness = 1 / (FalloffConstant + FalloffLinear * distance + FalloffQuadratic * distance * distance)
			float FalloffConstant, FalloffLinear, FalloffQuadratic;

			enum class LightFlags: uint32
			{
				None = 0,
				SkylightHack = 0xFFFF 	// Flag hacked in sky/ambient lights to be locally ignored. TODO: Is it used?
			} Flags;

			union
			{
				TextureInfoIndex Texture; //TODO: is it used?
				uint32 TextureRaw;
			};

			uint32 Owner; // TODO: is it used?
		};

		CUtlVector<WorldLight> Data;
		
		DECLARE_READ_WRITE(WorldLightsLump)
	} WorldLights;

	struct LeafFacesLump
	{
		CUtlVector<FaceIndex> Map;
		
		DECLARE_READ_WRITE(LeafFacesLump)	
	} LeafFaces;

	struct LeafBrushesLump
	{
		CUtlVector<BrushIndex> Map;

		DECLARE_READ_WRITE(LeafBrushesLump)
	} LeafBrushes;

	struct BrushesLump
	{
		struct Brush
		{
			BrushSide SideFirst;
			uint32 SideCount;

			ContentsFlag Contents;
		};

		CUtlVector<Brush> Data;
		
		DECLARE_READ_WRITE(BrushesLump)
	} Brushes;

	struct BrushSidesLump
	{
		struct BrushSide
		{
			PlaneIndex Plane; // Facing out of the leaf
			TextureInfoIndex Texture;
			DisplacementInfoIndex Displacement;

			bool16 Beveled;
		};

		CUtlVector<BrushSide> Data;
		
		DECLARE_READ_WRITE(BrushSidesLump)
	} BrushSides;

	struct AreasLump
	{
		struct Area
		{
			uint32 AreaportalCount;
			AreaportalIndex AreaportalFirst;
		};

		CUtlVector<Area> Data;

		DECLARE_READ_WRITE(AreasLump)
	} Areas;

	struct AreaportalsLump
	{
		struct Areaportal
		{
			uint16 PortalKey;	// Entities have a key called portalnumber (and in vbsp a variable
								// called areaportalnum) which is used
								// to bind them to the area portals by comparing with this value.

			AreaIndex OtherArea;

			ClipPortalVertexIndex VertexFirst;
			uint16 VertexCount;

			union 
			{
				PlaneIndex Plane;
				uint32 PlaneRaw;
			};
		};

		CUtlVector<Areaportal> Data;

		DECLARE_READ_WRITE(AreaportalsLump)
	} Areaportals;

	struct PortalsLump
	{
		struct Portal
		{
			PortalVertexIndex VertexFirst;
			uint32 VertexCount;
			union
			{
				PlaneIndex Plane;
				uint32 PlaneRaw;
			};

			ClusterIndex Cluster1, Cluster2;
		};

		CUtlVector<Portal> Data;

		DECLARE_READ_WRITE(PortalsLump)
	} Portals;

	struct ClustersLump
	{
		struct Cluster
		{
			ClusterPortalIndex PortalFirst;
			uint32 PortalCount;
		};

		CUtlVector<Cluster> Data;

		DECLARE_READ_WRITE(ClustersLump)
	} Clusters;

	struct PortalVertexesLump
	{
		CUtlVector<VertexIndex> Map;

		DECLARE_READ_WRITE(PortalVertexesLump)
	} PortalVertexes;

	struct ClusterPortalsLump
	{
		CUtlVector<PortalIndex> Map;

		DECLARE_READ_WRITE(ClusterPortalsLump)
	} ClusterPortals;

	struct DisplacementInfoLump
	{
		static constexpr uint8 PowerMin = 2, PowerMax = 4;
		


		static constexpr uint32 VertexesMax = DisplacementVertexesByPower(PowerMax);

		static constexpr uint32 TrianglesMax = DisplacementTrianglesByPower(PowerMax);
		
		// These define relative orientations of displacement neighbors.
		enum NeighborOrientation: uint8
		{
			ORIENTATION_CCW_0=0,
			ORIENTATION_CCW_90=1,
			ORIENTATION_CCW_180=2,
			ORIENTATION_CCW_270=3
		};

		// These denote where one dispinfo fits on another.
		// Note: tables are generated based on these indices so make sure to update
		//       them if these indices are changed.
		enum NeighborSpan: uint8
		{
			CORNER_TO_CORNER=0,
			CORNER_TO_MIDPOINT=1,
			MIDPOINT_TO_CORNER=2
		};

		
		struct DispSubNeighbor
		{
			DisplacementInfoIndex Neighbor;
			NeighborOrientation Orientation;

			NeighborSpan Span, NeighborSpan; // TODO: what is the difference?
			
		};

		struct DispCornerNeighbors
		{
			uint16 Neighbors[4];
			uint8 UsedNeighbors;
		};

		struct DispNeighbor
		{
			DispSubNeighbor Neighbor1, Neighbor2;
		};

		enum NeighborEdge
		{
			NEIGHBOREDGE_LEFT=0,
			NEIGHBOREDGE_TOP=1,
			NEIGHBOREDGE_RIGHT=2,
			NEIGHBOREDGE_BOTTOM=3
		};

		enum NeighborCorner
		{
			CORNER_LOWER_LEFT=0,
			CORNER_UPPER_LEFT=1,
			CORNER_UPPER_RIGHT=2,
			CORNER_LOWER_RIGHT=3
		};
		
		struct DispInfo
		{
			Vector StartPosition; // First corner

			DisplacementVertexIndex VertexesStart;
			uint32 VertexesCount() const noexcept
			{
				return DisplacementVertexesByPower(Power);
			}

			DisplacementTriangleIndex TrianglesStart;
			uint32 TrianglesCount() const
			{
				return DisplacementTrianglesByPower(Power);
			}

			uint32 Power;
			uint32 MinimalTesselation;

			float SmoothingAngle;
			ContentsFlag Contents;

			FaceIndex OriginalFace;
			
			DisplacementLightmapAlphaIndex LightmapAlphaStart; // Size = OriginalFace texture size

			DisplacementLightmapSamplePositionIndex LightmapSamplePosStart; // Size = OriginalFace texture size

			DispNeighbor EdgeNeighbors[4]; // Indexed by NeighborEdge
			DispCornerNeighbors CornerNeighbors[4]; // Indexed by NeighborCorner
			
			CBitVec<VertexesMax> AllowedVertexes;	// This is built based on the layout and sizes of our neighbors
													// and tells us which vertices are allowed to be active.
		};

		CUtlVector<DispInfo> Data;
		
		DECLARE_READ_WRITE(DisplacementInfoLump)
	} DisplacementInfo;

	struct OriginalFacesLump
	{
		CUtlVector<FacesLump::Face> Data;
		
		DECLARE_READ_WRITE(OriginalFacesLump)
	} OriginalFaces;

	struct PhysicalCollisionsLump
	{
		struct PhysicalCollision
		{
			union
			{
				BrushModelIndex Model;
				uint32 ModelRaw;
			};
			
			uint32 DataSize;
			uint32 KeyDataSize;
			uint32 SolidCount;
		};

		CUtlVector<PhysicalCollision> Data;

		DECLARE_READ_WRITE(PhysicalCollisionsLump)
	} PhysicalCollisions;

	struct VertexNormalsLump
	{
		CUtlVector<Vector> Normals;
		
		DECLARE_READ_WRITE(VertexNormalsLump)
	} VertexNormals;

	struct VertexNormalIndicesLump
	{
		CUtlVector<VertexNormalIndex> Map;

		DECLARE_READ_WRITE(VertexNormalIndicesLump)
	} VertexNormalIndices;

	struct DisplacementLightmapAlphaLump
	{
		CUtlVector<byte> Data; // Sequence of alpha bitmaps

		void CopyTo2DArray(uint32 start, CUTLArray2D<byte>& dest) const;

		void AddFrom2DArrayToEnd(CUTLArray2D<byte> const& source);

		DECLARE_READ_WRITE(DisplacementLightmapAlphaLump)
	} DisplacementLightmapAlpha;

	struct DisplacementVertexesLump
	{
		struct DisplacementVertex
		{
			Vector OffsetNormalized;
			vec_t OffsetDistance;
			float Alpha;
		};

		CUtlVector<DisplacementVertex> Data;

		DECLARE_READ_WRITE(DisplacementVertexesLump)
	} DisplacementVertexes;

	struct DisplacementLightmapSamplePositionsLump
	{
		struct SamplePosRaw
		{
			uint16 TriangleId; // 0 - 511
			byte BarycentricCoords[3];
		};

		struct SamplePos
		{
			uint16 TriangleId;
			Vector2D UV;
			Vector Position;
		};

		CUtlVector<byte> Data;

		void GenNRawSamplePositions(DisplacementLightmapSamplePositionIndex start, uint32 count,
			CUtlVector<SamplePosRaw>& out) const;

		// outSamplePositions is initialized inside
		static void GetSamplePositionsOf(
			CUtlVector<SamplePosRaw> const& rawSamples,
			bsp_v18::DisplacementInfoImplicit const& dispImplicitInfo,
			BspV18& bsp, DisplacementInfoIndex dispIndex, 
			CUTLArray2D<SamplePos>& outSamplePositions);

		DECLARE_READ_WRITE(DisplacementLightmapSamplePositionsLump)
	} DisplacementLightmapSamplePositions;

	struct CustomLump
	{
		
	} Custom; //TODO: implement

	struct LeafWaterDataLump
	{
		struct WaterData
		{
			vec_t Depth;
			vec_t MinZ;
			TextureInfoIndex SurfaceTexture;
		};

		CUtlVector<WaterData> Data;
		
		DECLARE_READ_WRITE(LeafWaterDataLump)
	} LeafWaterData;

	struct PrimitivesLump
	{
		enum class PrimitiveType: uint8
		{
			TriangleList = 0,
			TriangleStrip = 1
		};

		struct Primitive
		{
			PrimitiveType Type;

			PrimitiveIndexIndex IndexFirst;
			uint16 IndexCount;

			PrimitiveVertexIndex VertexFirst;
			uint16 VertexCount;
		};

		CUtlVector<Primitive> Data;

		DECLARE_READ_WRITE(PrimitivesLump)
	} Primitives;

	struct PrimitiveVertexesLump
	{
		CUtlVector<Vector> Vertexes;

		DECLARE_READ_WRITE(PrimitiveVertexesLump)
	} PrimitiveVertexes;

	struct PrimitiveIndicesLump
	{
		CUtlVector<PrimitiveVertexIndex> IndexToVertex;

		DECLARE_READ_WRITE(PrimitiveIndicesLump)
	} PrimitiveIndices;

	struct PakFileLump
	{
		
	} PakFile; //TODO: implement

	struct ClipPortalVertexesLump
	{
		CUtlVector<Vector> Vertexes;

		DECLARE_READ_WRITE(ClipPortalVertexesLump)
	} ClipPortalVertexes;

	struct CubemapsLump
	{
		struct Cubemap
		{
			int32 Origin[3];	// position of light snapped to the nearest integer
								// the filename for the vtf file is derived from the position

			uint8 SizeRaw;

			uint32 Size(uint32 defaultSize) const noexcept
			{
				if (SizeRaw == 0) return defaultSize;
				return 1<<(SizeRaw - 1);
			}
		};

		CUtlVector<Cubemap> Data;

		DECLARE_READ_WRITE(CubemapsLump)
	} Cubemaps;

	struct TextureStringDataLump
	{
		CUtlVector<CUtlString> Strings;

		DECLARE_READ_WRITE(TextureStringDataLump)
	} TextureStringData;

	// No need to use this, just index TextureStringData.Strings
	struct TextureStringTableLump
	{
		CUtlVector<uint32> Data;

		DECLARE_READ_WRITE(TextureStringTableLump)
	} TextureStringTable;

	struct OverlaysLump
	{
		union Face32
		{
			FaceIndex Face;
			uint32 Raw;
		};
		
		struct Overlay
		{
			uint32 Id; // Used internally in compiler, probably used to be used in older version of engine
			TextureInfoIndex Texture;
			uint16 FacesUsed;
			Face32 Faces[64];
			
			float U1, U2;
			float V1, V2; // Combos U1V1, U1V2, U2V2, U2V1 for 4 vertexes of plane

			Vector UVPoints[4];
			Vector Origin;
			Vector Normal;
		};

		CUtlVector<Overlay> Data;

		DECLARE_READ_WRITE(OverlaysLump)		
	} Overlays;

	struct LeafMinDistToWaterLump
	{
		CUtlVector<uint16> Map; // Index = LeafIndex

		DECLARE_READ_WRITE(LeafMinDistToWaterLump)
	} LeafMinDistToWater;

	struct FaceMacroTextureInfoLump
	{
		CUtlVector<TextureInfoIndex> Map; // Index = FaceIndex, 0xFFFF == no macrotexture

		DECLARE_READ_WRITE(FaceMacroTextureInfoLump)
	} FaceMacroTextureInfo;

	struct DisplacementTrianglesTagsLump
	{
		enum DisplacementTriangleTags: uint16
		{
			FlagSurface = 1 << 0,
			FlagWalkable = 1 << 1,
			FlagBuildable = 1 << 2
		};
		
		CUtlVector<DisplacementTriangleTags> TriTags;
		
		DECLARE_READ_WRITE(DisplacementTrianglesTagsLump)
	} DisplacementTrianglesTags;
};

namespace BspV18Utils
{
	namespace Displacement
	{

		using VertexIndex = uint16;

		struct VertexRef
		{
			Vector2D& UV;
			Vector& Position;
		};

		struct VertexConstRef
		{
			Vector2D const& UV;
			Vector const& Position;
		};

		class VertexTable
		{
			CUtlVector<Vector2D> _UVs;
			CUtlVector<Vector> _positions;
			BspV18::DisplacementInfoIndex _disp;

		public:
			VertexTable(BspV18& bsp, BspV18::DisplacementInfoIndex dispIndex):
				_disp(dispIndex)
			{
				auto& disp = bsp.DisplacementInfo.Data[dispIndex];
				auto& vertexes = bsp.DisplacementVertexes.Data;


				auto vertCount = disp.VertexesCount();
				auto vertOffset = disp.VertexesStart;

				_positions.EnsureCount(vertCount);
				
				for (auto vertIndex = 0; vertIndex < vertCount; vertIndex++)
				{
					auto vertRaw = vertexes[vertIndex + vertOffset];
					
					_positions[vertIndex] = vertRaw.OffsetNormalized * vertRaw.OffsetDistance;
				}
			}

			BspV18::DisplacementInfoIndex GetDisp() const { return _disp; }

			VertexRef Get(VertexIndex index)
			{
				return VertexRef {
					_UVs[index],
					_positions[index],
				};
			}

			VertexConstRef Get(VertexIndex index) const
			{
				return VertexConstRef {
					_UVs[index],
					_positions[index],
				};
			}
			
		};


		using TriangleIndex = uint16;

		struct Triangle
		{
			VertexIndex Vert1, Vert2, Vert3;
		};
		
		class TriangleTable
		{
			CUtlVector<Triangle> Triangles;
			BspV18::DisplacementInfoIndex _disp;


		public:

			BspV18::DisplacementInfoIndex GetDisp() const { return _disp;}
			
			Triangle& Get(TriangleIndex index)
			{
				return Triangles[index];
			}
			
			Triangle const& Get(TriangleIndex index) const
			{
				return Triangles[index];
			}
			
		};
		
	}

	
}