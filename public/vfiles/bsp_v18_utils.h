#pragma once
#include "bsp_v18.h"

namespace bsp_v18
{
	struct DisplacementInfoImplicit
	{
		struct Vertex
		{
			Vector LocalPosition;
			//Vector Normal;
			Vector2D UVRelative;
		}; 

		struct Triangle
		{
			Vertex* Vertexes[3]; // Vertex* is a non-owning reference here
		};

		CUtlVector<Vertex> Vertexes;
		CUtlVector<Triangle> Triangles;
		
	};

	DisplacementInfoImplicit::Vertex BarycentricInterpolateVertexes(DisplacementInfoImplicit::Vertex const* const vertexes[3], byte const barycentricRaw[3]);

	void GetImplicitDisplacementInfo(BspV18 const& bsp, BspV18::DisplacementInfoIndex displacement, DisplacementInfoImplicit& outImplicitInfo);

	
}
