#pragma once
#include <tuple>

template<typename... T>
struct TemplateTuple
{
	using Value = T;
};