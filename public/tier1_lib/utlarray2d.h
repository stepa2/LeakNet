#pragma once
#include "tier1_lib/utlmemory.h"

// 2-dimentional array
template<typename T>
class CUTLArray2D final
{
	CUtlMemory<T> _data;
	int _sizeX, _sizeY;
public:
	CUTLArray2D(int sizeX, int sizeY);
	bool IsValidPos(int x, int y) const;

	T& At(int x, int y);
	T const& At(int x, int y) const;

	int X() const noexcept { return _sizeX; }
	int Y() const noexcept { return _sizeY; }

	CUtlMemory<T>& RawMemory() noexcept { return _data; }
	CUtlMemory<T> const& RawMemory() const noexcept { return _data; }
	
private:
	size_t PosToIndex(int x, int y) const noexcept;
};

// 2-dimentional array
template<typename T, int SizeX, int SizeY>
class CUTLStaticArray2D final
{
	T _data[SizeX * SizeY]
#ifdef _DEBUG
		= {}
#endif
	;
public:
	CUTLStaticArray2D() = default;
	bool IsValidPos(int x, int y) const;

	T& At(int x, int y);
	T const& At(int x, int y) const;

	int X() const noexcept { return SizeX; }
	int Y() const noexcept { return SizeY; }

	T* Base() noexcept { return _data; }
	T const* Base() const noexcept { return _data; }
	
private:
	size_t PosToIndex(int x, int y) const noexcept;
};

