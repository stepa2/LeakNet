#pragma once
#include <algorithm>

template<typename T>
using FreeFunction = void (T*);

template<typename TObject, typename... TParams>
using CreateFunction = TObject* (TParams...);

template<typename TObject>
struct NonFreeingPointer
{
public:
	TObject* Inner;

	constexpr NonFreeingPointer(TObject* ptr) noexcept { Inner = ptr; }

	constexpr TObject* operator->() noexcept { return Inner; }
	constexpr TObject const* operator->() const noexcept { return Inner; }

	constexpr TObject& operator*() noexcept { return *Inner; }
	constexpr TObject const& operator*() const noexcept { return *Inner; }
};

template<typename TObject, FreeFunction<TObject> FreeFn>
struct ExplicitFreePointer : NonFreeingPointer<TObject>
{
	using Base = NonFreeingPointer<TObject>;
	
public:
	constexpr ExplicitFreePointer(TObject* ptr = nullptr) noexcept
		: Base(ptr) {}
	

	void Free() noexcept(noexcept(FreeFn(Base::Inner)))
	{
		FreeFn(Base::Inner);
	}

};

// TCreationParams is expected to be TemplateTuple
template<typename TObject, typename TCreationParams,
	CreateFunction<TObject, typename TCreationParams::Value...> CreateFn,
	FreeFunction<TObject> FreeFn>
struct ExplicitCreateFreePointer: ExplicitFreePointer<TObject, FreeFn>
{
	using Base = ExplicitFreePointer<TObject, FreeFn>;

public:

	constexpr ExplicitCreateFreePointer(TObject* ptr = nullptr) noexcept
		: Base(ptr) {}

	void Create(typename TCreationParams::Value... args)
	{
		Base::Inner = CreateFn(args);
	}
};
