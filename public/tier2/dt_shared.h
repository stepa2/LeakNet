//=========== (C) Copyright 1999 Valve, L.L.C. All rights reserved. ===========
//
// The copyright to the contents herein is the property of Valve, L.L.C.
// The contents may be used and/or copied only with the written permission of
// Valve, L.L.C., or in accordance with the terms and conditions stipulated in
// the agreement/contract under which the contents have been supplied.
//
// Purpose: 
//=============================================================================

#ifndef DATATABLE_SHARED_H
#define DATATABLE_SHARED_H

#ifdef _WIN32
#pragma once
#endif

#include "tier2/dt_common.h"

// ------------------------------------------------------------------------ //
// Client version
// ------------------------------------------------------------------------ //

#if defined (CLIENT_DLL)

#include "tier2/dt_recv.h"

#define PROPINFO(varName)							RECVINFO(varName)						
#define PROPINFO_DT(varName)						RECVINFO_DT(varName)					
#define PROPINFO_DT_NAME(varName, remoteVarName)	RECVINFO_DTNAME(varName,remoteVarName)	
#define PROPINFO_NAME(varName,remoteVarName)		RECVINFO_NAME(varName, remoteVarName)	

#define DataTableProp	RecvProp

#endif

// ------------------------------------------------------------------------ //
// Server version
// ------------------------------------------------------------------------ //

#if !defined (CLIENT_DLL)

#include "tier2/dt_send.h"

#define PROPINFO(varName)							SENDINFO(varName)			
#define PROPINFO_DT(varName)						SENDINFO_DT(varName)		
#define PROPINFO_DT_NAME(varName, remoteVarName)	SENDINFO_DT_NAME(varName, remoteVarName)
#define PROPINFO_NAME(varName,remoteVarName)		SENDINFO_NAME(varName,remoteVarName)

#define DataTableProp	SendProp

#endif

// Use these functions to setup your data tables.
DataTableProp PropFloat(
	char *pVarName,					// Variable name.
	int offset,						// Offset into container structure.
	int sizeofVar=SIZEOF_IGNORE,
	int nBits=32,					// Number of bits to use when encoding.
	int flags=0,
	float fLowValue=0.0f,			// For floating point, low and high values.
	float fHighValue=HIGH_DEFAULT	// High value. If HIGH_DEFAULT, it's (1<<nBits).
	);

DataTableProp PropVector(
	char *pVarName,
	int offset,
	int sizeofVar=SIZEOF_IGNORE,
	int nBits=32,					// Number of bits (for each floating-point component) to use when encoding.
	int flags=SPROP_NOSCALE,
	float fLowValue=0.0f,			// For floating point, low and high values.
	float fHighValue=HIGH_DEFAULT	// High value. If HIGH_DEFAULT, it's (1<<nBits).
	);

DataTableProp PropAngle(
	char *pVarName,
	int offset,
	int sizeofVar=SIZEOF_IGNORE,
	int nBits=32,
	int flags=0
	);

DataTableProp PropInt(
	char *pVarName,
	int offset,
	int sizeofVar=SIZEOF_IGNORE,	// Handled by SENDINFO macro.
	int nBits=-1,					// Set to -1 to automatically pick (max) number of bits based on size of element.
	int flags=0,
	int rightShift=0
	);

DataTableProp PropString(
	char *pVarName,
	int offset,
	int bufferLen,
	int flags=0
	);

DataTableProp PropEHandle(
	char *pVarName,
	int offset,
	int sizeofVar=SIZEOF_IGNORE );


// ------------------------------------------------------------------------ //
// Just wrappers to make shared code look easier...
// ------------------------------------------------------------------------ //

// Use these functions to setup your data tables.
DataTableProp PropFloat(
	char *pVarName,					// Variable name.
	int offset,						// Offset into container structure.
	int sizeofVar,
	int nBits,					// Number of bits to use when encoding.
	int flags,
	float fLowValue,			// For floating point, low and high values.
	float fHighValue	// High value. If HIGH_DEFAULT, it's (1<<nBits).
	)
{
#if !defined (CLIENT_DLL)
	return SendPropFloat( pVarName, offset, sizeofVar, nBits, flags, fLowValue, fHighValue );
#else
	return RecvPropFloat( pVarName, offset, sizeofVar, flags );
#endif
}

DataTableProp PropVector(
	char *pVarName,
	int offset,
	int sizeofVar,
	int nBits,					// Number of bits (for each floating-point component) to use when encoding.
	int flags,
	float fLowValue,			// For floating point, low and high values.
	float fHighValue	// High value. If HIGH_DEFAULT, it's (1<<nBits).
	)
{
#if !defined (CLIENT_DLL)
	return SendPropVector( pVarName, offset, sizeofVar, nBits, flags, fLowValue, fHighValue );
#else
	return RecvPropVector( pVarName, offset, sizeofVar, flags );
#endif
}

DataTableProp PropAngle(
	char *pVarName,
	int offset,
	int sizeofVar,
	int nBits,
	int flags
	)
{
#if !defined (CLIENT_DLL)
	return SendPropAngle( pVarName, offset, sizeofVar, nBits, flags );
#else
	return RecvPropFloat( pVarName, offset, sizeofVar, flags );
#endif
}

DataTableProp PropInt(
	char *pVarName,
	int offset,
	int sizeofVar,	// Handled by SENDINFO macro.
	int nBits,					// Set to -1 to automatically pick (max) number of bits based on size of element.
	int flags,
	int rightShift
	)
{
#if !defined (CLIENT_DLL)
	return SendPropInt( pVarName, offset, sizeofVar, nBits, flags, rightShift );
#else
	return RecvPropInt( pVarName, offset, sizeofVar, flags );
#endif
}

DataTableProp PropString(
	char *pVarName,
	int offset,
	int bufferLen,
	int flags
	)
{
#if !defined (CLIENT_DLL)
	return SendPropString( pVarName, offset, bufferLen, flags );
#else
	return RecvPropString( pVarName, offset, bufferLen, flags );
#endif
}

DataTableProp PropEHandle(
	char *pVarName,
	int offset,
	int sizeofVar )
{
#if !defined (CLIENT_DLL)
	return SendPropEHandle( pVarName, offset, sizeofVar );
#else
	return RecvPropEHandle( pVarName, offset, sizeofVar );
#endif
}

#endif // DATATABLE_SHARED_H

