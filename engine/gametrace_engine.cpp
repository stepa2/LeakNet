//========= Copyright � 1996-2003, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
//=============================================================================

#include "tier2/gametrace.h"
#include "server.h"
#include "tier2/eiface.h"


void CGameTrace::SetEdict( edict_t *pEdict )
{
	m_pEnt = serverGameEnts->EdictToBaseEntity( pEdict );
}


edict_t* CGameTrace::GetEdict() const
{
	return serverGameEnts->BaseEntityToEdict( m_pEnt );
}

