//========= Copyright (c) 1996-2002, Valve LLC, All rights reserved. ==========
//
// Purpose:
//
// $NoKeywords: $
//=============================================================================

#ifndef SND_CONVARS_H
#define SND_CONVARS_H

#if defined( _WIN32 )
#pragma once
#endif

#include "tier1_lib/convar.h"

extern	ConVar hisound;

#endif // SND_CONVARS_H
