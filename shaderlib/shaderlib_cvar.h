//========= Copyright � 1996-2003, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
//=============================================================================

#ifndef SHADERLIB_CVAR_H
#define SHADERLIB_CVAR_H
#ifdef _WIN32
#pragma once
#endif


#include "tier1_lib/interface.h"


void InitShaderLibCVars( CreateInterfaceFn cvarFactory );


#endif // SHADERLIB_CVAR_H
