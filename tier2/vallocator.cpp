
#include <malloc.h>
#include "tier2/vallocator.h"
#include "tier1_lib/basetypes.h"


VStdAllocator g_StdAllocator;


void* VStdAllocator::Alloc(unsigned long size)
{
	if(size)
	{
		void *ret = malloc(size);
		return ret;
	}
	else
		return 0;
}


void VStdAllocator::Free(void *ptr)
{
	free(ptr);
}

