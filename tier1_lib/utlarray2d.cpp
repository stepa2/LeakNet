#include "tier1_lib/utlarray2d.h"

template <typename T>
CUTLArray2D<T>::CUTLArray2D(int sizeX, int sizeY): _sizeX(sizeX), _sizeY(sizeY)
{
	_data(0, sizeX * sizeY);
}

template <typename T>
bool CUTLArray2D<T>::IsValidPos(int x, int y) const
{
	return x >= 0 && x < _sizeX && y >= 0 && y < _sizeY;
}

template <typename T>
size_t CUTLArray2D<T>::PosToIndex(int x, int y) const noexcept
{
	AssertMsg2(IsValidPos(x,y), "Invalid position: (%d,%d)",x,y);

	return x + y * _sizeX;
}

template <typename T>
T& CUTLArray2D<T>::At(int x, int y)
{
	return _data[PosToIndex(x,y)];
}

template <typename T>
T const& CUTLArray2D<T>::At(int x, int y) const
{
	return _data[PosToIndex(x,y)];
}

template <typename T, int SizeX, int SizeY>
bool CUTLStaticArray2D<T, SizeX, SizeY>::IsValidPos(int x, int y) const
{
	return x >= 0 && x < SizeX && y >= 0 && y < SizeY;
}

template <typename T, int SizeX, int SizeY>
size_t CUTLStaticArray2D<T, SizeX, SizeY>::PosToIndex(int x, int y) const noexcept
{
	AssertMsg2(IsValidPos(x,y), "Invalid position: (%d,%d)",x,y);

	return x + y * SizeX;
}

template <typename T, int SizeX, int SizeY>
T& CUTLStaticArray2D<T, SizeX, SizeY>::At(int x, int y)
{
	return _data[PosToIndex(x,y)];
}

template <typename T, int SizeX, int SizeY>
T const& CUTLStaticArray2D<T, SizeX, SizeY>::At(int x, int y) const
{
	return _data[PosToIndex(x,y)];
}