#include "vfiles/bsp_v18_utils.h"

#include "vfiles/bsp_v18.h"

// Displacement lightmap alpha

void BspV18::DisplacementLightmapAlphaLump::CopyTo2DArray(uint32 start, CUTLArray2D<byte>& dest) const
{
	Assert(Data.Count() > start + dest.RawMemory().Count());
	memcpy(dest.RawMemory().Base(), &Data[start], dest.RawMemory().Count());
}

void BspV18::DisplacementLightmapAlphaLump::AddFrom2DArrayToEnd(CUTLArray2D<byte> const& source)
{
	Data.AddMultipleToTail(source.RawMemory().Count(), source.RawMemory().Base());
}

// Displacement sample postitions

void BspV18::DisplacementLightmapSamplePositionsLump::GenNRawSamplePositions(
	DisplacementLightmapSamplePositionIndex start, uint32 count, 
	CUtlVector<SamplePosRaw>& out) const
{
	out.EnsureCapacity(count);

	uint32 offset = start;

	for (uint32 i = 0; i < count; i++)
	{
		SamplePosRaw element;

		uint16 triangle = Data[offset];
		offset++;
		if (triangle == 255)
		{
			triangle += Data[offset];
			offset++;
		}

		for (size_t j = 0; j < 3; j++)
			element.BarycentricCoords[j] = Data[offset + j];

		offset += 3;
		
		out.AddToTail(element);
	}

}

Vector2D WorldPosToTextureCoords(Vector const& world, 
	BspV18::TextureInfoLump::WorldToTexsize const& mappingU, 
	BspV18::TextureInfoLump::WorldToTexsize const& mappingV,
	Vector2D const& negativeOffset)
{
	Vector2D result(world.Dot(mappingU.WorldScale),world.Dot(mappingV.WorldScale));
	result += Vector2D(mappingU.Constant, mappingV.Constant);
	
	return result - negativeOffset;
}



void bsp_v18::GetImplicitDisplacementInfo(BspV18 const& bsp, 
                                          BspV18::DisplacementInfoIndex displacementIndex, 
                                          bsp_v18::DisplacementInfoImplicit& outImplicitInfo)
{
	const auto& disp = bsp.DisplacementInfo.Data[displacementIndex];
	const auto& dispFace = bsp.Faces.Data[disp.OriginalFace];
	const auto& dispTexture = bsp.TextureInfo.Data[dispFace.Texture];

	Assert(dispFace.EdgeCount == 4); // For now only 4-sided displacements are supported

	auto vertexCount = disp.VertexesCount();
	auto vertexCountSide = (1 << disp.Power) + 1u;

	outImplicitInfo.Vertexes.EnsureCapacity(outImplicitInfo.Vertexes.Count() + vertexCount);
	outImplicitInfo.Triangles.EnsureCapacity(outImplicitInfo.Triangles.Count() + disp.TrianglesCount());
	
	Vector2D uvs[4];

	{
		const auto& edge1 = bsp.Edges.Data[dispFace.EdgeFirst];
		const auto& edge2 = bsp.Edges.Data[dispFace.EdgeFirst + 2];

		const auto& vertex1 = bsp.Vertexes.Vertexes[edge1.Vertex1];
		const auto& vertex2 = bsp.Vertexes.Vertexes[edge1.Vertex2];
		const auto& vertex3 = bsp.Vertexes.Vertexes[edge2.Vertex2];
		const auto& vertex4 = bsp.Vertexes.Vertexes[edge2.Vertex1];

		Vector const* vertexes[] = {&vertex1, &vertex2, &vertex3, &vertex4};
	
		Vector2D faceUvMin(dispFace.TextureUMin, dispFace.TextureVMin);

		for (size_t i = 0; i < 4; i++)
			uvs[i] = WorldPosToTextureCoords(*vertexes[i], 
				dispTexture.LightmapU, dispTexture.LightmapV, faceUvMin);
	}

	for (BspV18::DisplacementVertexIndex i1 = 0; i1 < disp.VertexesCount(); i1++)
	{
		auto i = i1 + disp.VertexesStart;
		auto vertex = bsp.DisplacementVertexes.Data[i];
		auto implicitVertex = bsp_v18::DisplacementInfoImplicit::Vertex();
		implicitVertex.LocalPosition = vertex.OffsetNormalized * vertex.OffsetDistance;

		implicitVertex.UVRelative = Vector2D( // TODO: correct order?
			(float)(i1 % vertexCountSide),
			(float)(i1 / vertexCountSide) // Integer division here
		)/(float)vertexCountSide;

		outImplicitInfo.Vertexes.AddToTail(implicitVertex);
	}

	for (BspV18::DisplacementTriangleIndex i1 = 0; i1 < disp.TrianglesCount(); i1++)
	{
		auto i = i1 + disp.TrianglesStart;

		BspV18::DisplacementVertexIndex vertex1 = 0, vertex2 = 0, vertex3 = 0;

		switch (i1 % 4)
		{
		case 0:
			vertex1 = i1;
			vertex2 = i1 + vertexCountSide;
			vertex3 = i1 + vertexCountSide + 1;
			break;
		case 1:
			vertex1 = i1;
			vertex2 = i1 + vertexCountSide + 1;
			vertex3 = i1 + 1;
			break;
		case 2:
			vertex1 = i1;
			vertex2 = i1 + vertexCountSide;
			vertex3 = i1 + 1;
			break;
		case 3:
			vertex1 = i1 + 1;
			vertex2 = i1 + vertexCountSide;
			vertex3 = i1 + vertexCountSide + 1;
			break;
		NO_DEFAULT
		}

		DisplacementInfoImplicit::Triangle triangle{};
		triangle.Vertexes[0] = &outImplicitInfo.Vertexes[vertex1];
		triangle.Vertexes[1] = &outImplicitInfo.Vertexes[vertex2];
		triangle.Vertexes[2] = &outImplicitInfo.Vertexes[vertex3];
		
		outImplicitInfo.Triangles.AddToTail(triangle);
	}
	
}

bsp_v18::DisplacementInfoImplicit::Vertex bsp_v18::BarycentricInterpolateVertexes(
	DisplacementInfoImplicit::Vertex const* const vertexes[3], byte const barycentricRaw[3])
{
	bsp_v18::DisplacementInfoImplicit::Vertex result{};

	for (size_t i = 0; i < 3; i++)
	{
		float barycentric = barycentricRaw[i] / 255.9f;
		result.UVRelative += vertexes[i]->UVRelative * barycentric;
		result.LocalPosition   += vertexes[i]->LocalPosition   * barycentric;
	}

	return result;
}

void BspV18::DisplacementLightmapSamplePositionsLump::GetSamplePositionsOf(
	CUtlVector<SamplePosRaw> const& rawSamples,
	bsp_v18::DisplacementInfoImplicit const& dispImplicitInfo,
	BspV18& bsp, DisplacementInfoIndex dispIndex,
	CUTLArray2D<SamplePos>& outSamplePositions)
{
	auto& disp = bsp.DisplacementInfo.Data[dispIndex];
	auto& dispVertexes = bsp.DisplacementVertexes.Data;

	auto& dispFace = bsp.Faces.Data[disp.OriginalFace];

	outSamplePositions = CUTLArray2D<SamplePos>(dispFace.TextureUSize + 1, dispFace.TextureVSize + 1);
	
	auto sampleStart = disp.LightmapSamplePosStart;
	
	for (int y = 0; y < outSamplePositions.Y(); y++)
		for (int x = 0; x < outSamplePositions.X(); x++)
		{
			SamplePosRaw rawSample = rawSamples[y * outSamplePositions.X() + x];
			SamplePos& outSample = outSamplePositions.At(x, y);

			const auto& tri = dispImplicitInfo.Triangles[rawSample.TriangleId];

			auto resultVertex = BarycentricInterpolateVertexes( tri.Vertexes, rawSample.BarycentricCoords);
			//outSample.Position = re
			
		}
}

/*	binary.EnsureCapacity(Data.Count() * 5); // Raw sample positions are 4-5 bytes

	for (size_t i = 0; i < Data.Count(); i++)
	{
		SamplePosRaw const& element = Data[i];

		if (element.TriangleId > 511)
		{
			Error("BSP: DisplacementLightmapSamplePositions: triangle id > 511 "
					  "(more than 511 triangles on disp)");
			continue;
		}
		else if(element.TriangleId > 255)
		{
			binary.PutUnsignedChar(255);
			binary.PutUnsignedChar(255 - element.TriangleId);
		}
		else
			binary.PutUnsignedChar(element.TriangleId);

		binary.Put(element.BarycentricCoords, sizeof(element.BarycentricCoords));
	}*/
