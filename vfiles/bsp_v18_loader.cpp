#include "vfiles/bsp_v18.h"

template<typename T>
bool BinaryToTSeq(CUtlBuffer& binary, CUtlVector<T>& out)
{
	if (binary.Size() == 0) return true; // Read nothing
	
	if (binary.Size() % sizeof(T) != 0)
	{
		Error("BSP lump has invalid Count");
		return false;
	}

	out.CopyArray((T*)binary.Base(), binary.Size() / sizeof(T));
	return true;
}

template<typename T>
void TSeqToBinary(CUtlVector<T> const& in, CUtlBuffer& outBinary)
{
	if(in.Count() == 0) return;
	outBinary.Put((T*)in.Base(), in.Count() * sizeof(T));
}

#define GENERATE_BINARY_VECTOR_READ_WRITE(LUMP, ELEMENT_VAR) \
	bool BspV18::LUMP::Read(CUtlBuffer& binary, LUMP& out) \
	{ \
		return BinaryToTSeq(binary, out.ELEMENT_VAR); \
	} \
	\
	void BspV18::LUMP::Write(CUtlBuffer& binary) \
	{ \
		TSeqToBinary(ELEMENT_VAR, binary); \
	}


// Entities

// Planes
GENERATE_BINARY_VECTOR_READ_WRITE(PlanesLump, Data)

// Texture data
GENERATE_BINARY_VECTOR_READ_WRITE(TextureDataLump, Data)

// Vertexes
GENERATE_BINARY_VECTOR_READ_WRITE(VertexesLump, Vertexes)

// Visibility
GENERATE_BINARY_VECTOR_READ_WRITE(VisibilityLump, Binary) // TODO: Will be custom reader/writer

// Visibility nodes
GENERATE_BINARY_VECTOR_READ_WRITE(NodesLump, Data)

// Texture info
GENERATE_BINARY_VECTOR_READ_WRITE(TextureInfoLump, Data)

// Faces
GENERATE_BINARY_VECTOR_READ_WRITE(FacesLump, Data)

// Lighting data
GENERATE_BINARY_VECTOR_READ_WRITE(LightingDataLump, Colors)

// Occulder

// Utility macro to check lump size
#define CHECK_OCCULDER_LUMP_SIZE \
	if(binary.Size() < required_size) \
			return Error("BSP Occlusion lump has invalid size"), false;

bool BspV18::OcclusionLump::Read(CUtlBuffer& binary, OcclusionLump& out)
{
	size_t required_size = sizeof(uint32);
	
	if (binary.Size() < sizeof(uint32) * 3)
		return Error("BSP Occlusion lump has invalid size"), false;

	{
		uint32 occulderCount = binary.GetUnsignedInt();
		required_size += occulderCount * sizeof(OcculderData);
	
		CHECK_OCCULDER_LUMP_SIZE

		out.Occulders.SetCount(occulderCount);
		binary.Get(out.Occulders.Base(), occulderCount * sizeof(occulderCount));
	}

	{
		required_size += sizeof(uint32);
		CHECK_OCCULDER_LUMP_SIZE

		uint32 polygonCount = binary.GetUnsignedInt();

		required_size += polygonCount * sizeof(OcculderPolygon);
		CHECK_OCCULDER_LUMP_SIZE

		out.Polygons.SetCount(polygonCount);
		binary.Get(out.Polygons.Base(), polygonCount * sizeof(OcculderPolygon));
	}

	{
		required_size += sizeof(uint32);
		CHECK_OCCULDER_LUMP_SIZE

		uint32 vertexIndicesCount = binary.GetUnsignedInt();

		required_size += vertexIndicesCount * sizeof(OcclusionVertexIndex);
		CHECK_OCCULDER_LUMP_SIZE

		out.VertexIndices.SetCount(vertexIndicesCount);
		binary.Get(out.VertexIndices.Base(), vertexIndicesCount * sizeof(OcclusionVertexIndex));
	}

	return true;
}

#undef CHECK_OCCULDER_LUMP_SIZE

void BspV18::OcclusionLump::Write(CUtlBuffer& binary)
{
	binary.PutUnsignedInt(Occulders.Count());
	binary.Put(Occulders.Base(), Occulders.Count() * sizeof(OcculderData));

	binary.PutUnsignedInt(Polygons.Count());
	binary.Put(Polygons.Base(), Polygons.Count() * sizeof(OcculderPolygon));

	binary.PutUnsignedInt(VertexIndices.Count());
	binary.Put(VertexIndices.Base(), VertexIndices.Count() * sizeof(OcclusionVertexIndex));
}

// Leafs
GENERATE_BINARY_VECTOR_READ_WRITE(LeafsLump, Data)

// Edges
GENERATE_BINARY_VECTOR_READ_WRITE(EdgesLump, Data)

// Surface edges
GENERATE_BINARY_VECTOR_READ_WRITE(SurfaceEdgesLump, Edges)

// Brush models
GENERATE_BINARY_VECTOR_READ_WRITE(BrushModelsLump, Data)

// World lights
GENERATE_BINARY_VECTOR_READ_WRITE(WorldLightsLump, Data)

// Leaf faces
GENERATE_BINARY_VECTOR_READ_WRITE(LeafFacesLump, Map)

// Leaf brushes
GENERATE_BINARY_VECTOR_READ_WRITE(LeafBrushesLump, Map)

// Brushes
GENERATE_BINARY_VECTOR_READ_WRITE(BrushesLump, Data)

// Brush sides
GENERATE_BINARY_VECTOR_READ_WRITE(BrushSidesLump, Data)

// Areas
GENERATE_BINARY_VECTOR_READ_WRITE(AreasLump, Data)

// Areaportals
GENERATE_BINARY_VECTOR_READ_WRITE(AreaportalsLump, Data)

// Portals
GENERATE_BINARY_VECTOR_READ_WRITE(PortalsLump, Data)

// Clusters
GENERATE_BINARY_VECTOR_READ_WRITE(ClustersLump, Data)

// Portal vertexes
GENERATE_BINARY_VECTOR_READ_WRITE(PortalVertexesLump, Map)

// Cluster portals
GENERATE_BINARY_VECTOR_READ_WRITE(ClusterPortalsLump, Map)

// Displacement infos
GENERATE_BINARY_VECTOR_READ_WRITE(DisplacementInfoLump, Data)

// Original faces
GENERATE_BINARY_VECTOR_READ_WRITE(OriginalFacesLump, Data)

// Brush physical collisions
GENERATE_BINARY_VECTOR_READ_WRITE(PhysicalCollisionsLump, Data)

// Vertex normals
GENERATE_BINARY_VECTOR_READ_WRITE(VertexNormalsLump, Normals)

// Vertex normals indices
GENERATE_BINARY_VECTOR_READ_WRITE(VertexNormalIndicesLump, Map)


// Displacement alpha
GENERATE_BINARY_VECTOR_READ_WRITE(DisplacementLightmapAlphaLump, Data)

// Displacement vertexes
GENERATE_BINARY_VECTOR_READ_WRITE(DisplacementVertexesLump, Data)

// Displacement sample postitions
GENERATE_BINARY_VECTOR_READ_WRITE(DisplacementLightmapSamplePositionsLump, Data)

// Leaf water data
GENERATE_BINARY_VECTOR_READ_WRITE(LeafWaterDataLump, Data)

// Primitives
GENERATE_BINARY_VECTOR_READ_WRITE(PrimitivesLump, Data)

// Primitives vertexes
GENERATE_BINARY_VECTOR_READ_WRITE(PrimitiveVertexesLump, Vertexes)

// Primitives indexes
GENERATE_BINARY_VECTOR_READ_WRITE(PrimitiveIndicesLump, IndexToVertex)

// Clip portal vertexes
GENERATE_BINARY_VECTOR_READ_WRITE(ClipPortalVertexesLump, Vertexes)

// Cubemaps
GENERATE_BINARY_VECTOR_READ_WRITE(CubemapsLump, Data)

// Texture string data
bool BspV18::TextureStringDataLump::Read(CUtlBuffer& binary, TextureStringDataLump& out)
{
	while (binary.TellGet() < binary.Size())
	{
		char buffer[1024]; // TODO: dynamic allocation
		binary.GetString(buffer, ARRAYSIZE(buffer));
		if (!binary.IsValid()) return false;

		out.Strings.AddToTail(buffer);
	}
	
	return true;
}
void BspV18::TextureStringDataLump::Write(CUtlBuffer& binary)
{
	for (size_t i = 0; i < Strings.Count(); i++)
	{
		binary.PutString(Strings[i]);
	}
}

// Texture string table
GENERATE_BINARY_VECTOR_READ_WRITE(TextureStringTableLump, Data) // Not used by *this* version of LeakNet.
																//Backwards compatility for read-write
														
// Overlays
GENERATE_BINARY_VECTOR_READ_WRITE(OverlaysLump, Data)

// Minimal distance to water by leaf
GENERATE_BINARY_VECTOR_READ_WRITE(LeafMinDistToWaterLump, Map)

// Macrotextures by face
GENERATE_BINARY_VECTOR_READ_WRITE(FaceMacroTextureInfoLump, Map)

// Displacement triangles flags
GENERATE_BINARY_VECTOR_READ_WRITE(DisplacementTrianglesTagsLump, TriTags)